﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IKAA_Lab3_EC154
{
    public class PixelClassYUV
    {
        public double Y; 
        public double U; 
        public double V; 

        public PixelClassYUV()
        {
            Y = 0;
            U = 0;
            V = 0;
        }

        //konstruktors + pareja no RGB uz YUV
        public PixelClassYUV(byte r, byte g, byte b)
        {
            Y = Convert.ToByte(0.299 * r + 0.587 * g + 0.114 * b);
            U = Convert.ToByte(-0.14713 * r - 0.28886 * g + 0.436 * b + 128);
            V = Convert.ToByte(0.615 * r - 0.51499 * g - 0.10001 * b + 128);

        }
    }
}
