﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IKAA_Lab3_EC154
{
    public class PixelClassRGB
    {
        public byte R, G, B, I;
        public PixelClassRGB() //konstruktoers tuksam pikselim
        {
            R = 0;
            G = 0;
            B = 0;
            I = 0;
        }
        public PixelClassRGB(byte r, byte g, byte b) //konstr. kas ieraksta pikseli vertibas
        {
            R = r;
            G = g;
            B = b;
            I = (byte)Math.Round(0.0722f * b + 0.715f * g + 0.212f * r);
        }

        public PixelClassRGB HSVtoRGB(int h, byte s, byte v)
        {
            byte r = 0;
            byte g = 0;
            byte b = 0;

            int Hi = Convert.ToInt32(h / 60);
            byte Vmin = Convert.ToByte((255 - s) * v / 255);
            int a = Convert.ToInt32((v - Vmin) * (h % 60) / 60);
            byte Vinc = Convert.ToByte(Vmin + a);
            byte Vdec = Convert.ToByte(v - a);

            //rgb atkarigs no Hi(tabula prezentacija)
            switch (Hi)
            {
                case 0: { r = v; g = Vinc; b = Vmin; break; }
                case 1: { r = Vdec; g = v; b = Vmin; break; }
                case 2: { r = Vmin; g = v; b = Vinc; break; }
                case 3: { r = Vmin; g = Vdec; b = v; break; }
                case 4: { r = Vinc; g = Vmin; b = v; break; }
                case 5: { r = v; g = Vmin; b = Vdec; break; }

            }
            PixelClassRGB pixRGB = new PixelClassRGB(r, g, b);
            return pixRGB;
        }

        public PixelClassRGB CMYKtoRGB(float C, float M, float Y, float K)
        {
            byte r = 0;
            byte g = 0;
            byte b = 0;

            r = (byte)(255 * (1 - C) * (1 - K));
            g = (byte)(255 * (1 - M) * (1 - K));
            b = (byte)(255 * (1 - Y) * (1 - K));

            PixelClassRGB pixRGB2 = new PixelClassRGB(r,g,b);
            return pixRGB2;
        }

        public PixelClassRGB YUVtoRGB(double Y, double U, double V)
        {
            byte r = 0;
            byte g = 0;
            byte b = 0;

            r = (byte)(Y + 1.13983 * (V - 128));
            g = (byte)(Y - 0.39465 * (U - 128) - 0.58060 * (V - 128));
            b = (byte)(Y + 2.03211 * (U - 128));

            PixelClassRGB pixRGB2 = new PixelClassRGB(r, g, b);
            return pixRGB2;
        }
    }
 }
