﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IKAA_Lab3_EC154
{
    public partial class IKAA_Lab3_EC154 : Form
    {
        public ImageClass imageClass = new ImageClass();
        public PixelClassRGB pixYUV = new PixelClassRGB();

        public IKAA_Lab3_EC154()
        {
            InitializeComponent();
        }
        private void open_button_Click(object sender, EventArgs e)
        {
            //parbbauda vai lietotajs dialoga ir izvelejies failu un nospiedis ok
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //ielade izveleto failu picturebox1
                Stopwatch timeToMake = new Stopwatch(); //izpildes laika skaititajs
                timeToMake.Start();

                pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);
                Bitmap bmp = (Bitmap)pictureBox1.Image.Clone();
                imageClass.ReadImage(bmp);
                pictureBox2.Image = imageClass.DrawImage(imageClass.img1, "RGB");
                imageClass.hst1.DrawHistogram(chart1);
                imageClass.hst1.DrawHistogram(chart2);

                //foolproof
                RGB.Checked = true;

                timeToMake.Stop(); //taimera darbibas beigas
                timeLabel.Text = String.Format("Izpildes laiks: {0}", timeToMake.Elapsed);
            }
        }
        private Point ConvertXY(int x, int y, PictureBox pb) //lai izlabotu zoom 
        {
            Point p = new Point();
            int imgwidth = pb.Image.Width;
            int imgheight = pb.Image.Height;
            int boxwidth = pb.Width;
            int boxheight = pb.Height;

            //nosakam proporcionalitates koeficentu
            double kx = (double)imgwidth / boxwidth;
            double ky = (double)imgheight / boxheight;
            double k = Math.Max(kx, ky); //max saspiesanas vertiba
                                         //rekinam nobidi
            double nobideX = (boxwidth * k - imgwidth) / 2f; //2.0, floating point 
            double nobideY = (boxheight * k - imgheight) / 2f;

            p.X = Convert.ToInt32(Math.Max(Math.Min( // saglaba attela izmerus, lai "neaizpeldetu"               
                Math.Round(x * k - nobideX), imgwidth - 1), 0));
            p.Y = Convert.ToInt32(Math.Max(Math.Min(
                Math.Round(y * k - nobideY), imgheight - 1), 0));
            return p;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Image != null)
            {
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "JPG(*.jpg)|*.jpg|PNG(*.png) | *.png"; //piedavatie attela formati
                if (save.ShowDialog() == DialogResult.OK)
                {
                    pictureBox2.Image.Save(save.FileName);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //foolcheck, lai lietotajs nemeginatu invertet tuksumu
            if (pictureBox2.Image != null)
            {
                Stopwatch timeToMake = new Stopwatch(); //izpildes laika skaititajs
                timeToMake.Start();
                int r, g, b; //pagaidu vertibas, ertibas pec
                             //cikli, kas iziet cauri pikseliem
                for (int i = 0; i < pictureBox2.Image.Width; i++)
                {
                    for (int j = 0; j < pictureBox2.Image.Height; j++)
                    {//ieraksta invertetas piksela vertibas
                        r = 255 - ((Bitmap)pictureBox2.Image).GetPixel(i, j).R;
                        g = 255 - ((Bitmap)pictureBox2.Image).GetPixel(i, j).G;
                        b = 255 - ((Bitmap)pictureBox2.Image).GetPixel(i, j).B;
                        //iezimejam jaunu pikseli
                        ((Bitmap)pictureBox2.Image).SetPixel(i, j, Color.FromArgb(r, g, b));
                    }
                }
                timeToMake.Stop(); //taimera darbibas beigas
                timeLabel.Text = String.Format("Izpildes laiks: {0}", timeToMake.Elapsed);
                pictureBox2.Refresh(); //refreso picture box
            }
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            //foolproof (again), parbauda vai lietotajs neklikskina uz tuiksas bildes
            if (pictureBox1.Image != null)
            {
                //piksela noteiksana
                Point p = new Point();
                p = ConvertXY(e.X, e.Y, pictureBox1);
                Coord.Text = "X,Y Koordin.: " + Convert.ToString(p.X) + "," + Convert.ToString(p.Y);
                //piksela rgb krasas noteiksana
                Color color1 = ((Bitmap)pictureBox1.Image).GetPixel(p.X, p.Y);
                rgb_val1.Text = string.Format("RGB PB1:  R:{0}, G:{1}, B:{2}", color1.R, color1.G, color1.B); //rgb vertibas izveletaja pikseli
                Color color2 = ((Bitmap)pictureBox2.Image).GetPixel(p.X, p.Y);
                rgb_val2.Text = string.Format("RGB PB2:  R:{0}, G:{1}, B:{2}", color2.R, color2.G, color2.B); //rgb vertibas izveletaja pikseli
                toolTip1.SetToolTip(this.pictureBox1, rgb_val1.Text + "," + '\n' + rgb_val2.Text + '\n' + Coord.Text);

                C_val1.Text = string.Format("R:{0}", color1.R); C_val1.ForeColor = Color.Red;
                C_val2.Text = string.Format("G:{0}", color1.G); C_val2.ForeColor = Color.Green;
                C_val3.Text = string.Format("B:{0}", color1.B); C_val3.ForeColor = Color.Blue;
              
            }
        }

        private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            if (pictureBox2.Image != null)
            {
                
                Point m = new Point();
                m = ConvertXY(e.X, e.Y, pictureBox2);
                Color color1 = ((Bitmap)pictureBox1.Image).GetPixel(m.X, m.Y);
                rgb_val1.Text = string.Format("RGB PB1:  R:{0}, G:{1}, B:{2}", color1.R, color1.G, color1.B); //rgb vertibas izveletaja pikseli
                Color color2 = ((Bitmap)pictureBox2.Image).GetPixel(m.X, m.Y);
                rgb_val2.Text = string.Format("RGB PB2:  R:{0}, G:{1}, B:{2}", color2.R, color2.G, color2.B); //rgb vertibas izveletaja pikseli
                toolTip1.SetToolTip(this.pictureBox2, rgb_val1.Text + "," + '\n' + rgb_val2.Text + '\n' + Coord.Text);
                Coord.Text = "X,Y Koordin.: " + Convert.ToString(m.X) + "," + Convert.ToString(m.Y);
                ((Bitmap)pictureBox2.Image).SetPixel(m.X, m.Y, button4.BackColor);
                C_val1.Text = string.Format("R:{0}", color2.R); C_val1.ForeColor = Color.Red;
                C_val2.Text = string.Format("G:{0}", color2.G); C_val2.ForeColor = Color.Green;
                C_val3.Text = string.Format("B:{0}", color2.B); C_val3.ForeColor = Color.Blue;
                Ch1.Text = string.Format("Y:{0} U:{1} V:{2}", pixYUV.YUVtoRGB(color2.R,color2.G,color2.B).R, pixYUV.YUVtoRGB(color2.R, color2.G, color2.B).G, pixYUV.YUVtoRGB(color2.R, color2.G, color2.B).B);
                Ch2.Text = string.Format("C:{0} M:{1} Y:{2} K:{3}", pixYUV.CMYKtoRGB(color2.R,color2.G,color2.B,color2.A).R, pixYUV.CMYKtoRGB(color2.R, color2.G, color2.B, color2.A).G, pixYUV.CMYKtoRGB(color2.R, color2.G, color2.B, color2.A).B, pixYUV.CMYKtoRGB(color2.R, color2.G, color2.B, color2.A).I);
                pictureBox2.Refresh();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog1 = new ColorDialog();
            toolTip1.SetToolTip(this.button4, "Izvēlēties krāsu no Color dialog, kurā iekrāsot pikseļus");
            colorDialog1.AllowFullOpen = false; // iespeja izveleties pasam custom krasu
            if (colorDialog1.ShowDialog() == DialogResult.OK) //ja ok tad iekraso pikseli izveletaja krasa
                button4.BackColor = colorDialog1.Color;
        }

        private void RGB_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Image != null)
            {
                RadioButton rb = (RadioButton)sender;
                pictureBox2.Image = imageClass.DrawImage(imageClass.img1, rb.Text);
                Current_label.Text = "Aktīvā krāsu sistēma:  " + rb.Text;
            }
        }

        private void filtrs1ToolStripMenuItem_Click(object sender, EventArgs e) 
        {
            imageClass.Filter();
            pictureBox2.Image = imageClass.DrawImage(imageClass.img2,"filter1");
        }
    }
}
