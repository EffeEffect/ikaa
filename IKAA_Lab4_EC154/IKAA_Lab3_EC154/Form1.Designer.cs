﻿namespace IKAA_Lab3_EC154
{
    partial class IKAA_Lab3_EC154
    {
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.open_button = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button4 = new System.Windows.Forms.Button();
            this.Coord = new System.Windows.Forms.Label();
            this.rgb_val1 = new System.Windows.Forms.Label();
            this.rgb_val2 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.timeLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.U = new System.Windows.Forms.RadioButton();
            this.YUV = new System.Windows.Forms.RadioButton();
            this.Y = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.CMYK = new System.Windows.Forms.RadioButton();
            this.V = new System.Windows.Forms.RadioButton();
            this.S = new System.Windows.Forms.RadioButton();
            this.H = new System.Windows.Forms.RadioButton();
            this.HSV = new System.Windows.Forms.RadioButton();
            this.B = new System.Windows.Forms.RadioButton();
            this.G = new System.Windows.Forms.RadioButton();
            this.R = new System.Windows.Forms.RadioButton();
            this.RGB = new System.Windows.Forms.RadioButton();
            this.C_val1 = new System.Windows.Forms.Label();
            this.C_val2 = new System.Windows.Forms.Label();
            this.C_val3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Ch2 = new System.Windows.Forms.Label();
            this.Ch1 = new System.Windows.Forms.Label();
            this.Current_label = new System.Windows.Forms.Label();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.filtrs1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filtrs2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filtrs3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // open_button
            // 
            this.open_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.open_button.Location = new System.Drawing.Point(1004, 28);
            this.open_button.Name = "open_button";
            this.open_button.Size = new System.Drawing.Size(205, 29);
            this.open_button.TabIndex = 0;
            this.open_button.Text = "OPEN";
            this.open_button.UseVisualStyleBackColor = true;
            this.open_button.Click += new System.EventHandler(this.open_button_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(1004, 72);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(205, 29);
            this.button2.TabIndex = 1;
            this.button2.Text = "SAVE";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(1004, 118);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(205, 29);
            this.button3.TabIndex = 2;
            this.button3.Text = "INVERT";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox1.Location = new System.Drawing.Point(35, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(450, 450);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox2.Location = new System.Drawing.Point(506, 30);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(450, 450);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseDown);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(1098, 564);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(43, 38);
            this.button4.TabIndex = 5;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Coord
            // 
            this.Coord.AutoSize = true;
            this.Coord.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Coord.Location = new System.Drawing.Point(785, 513);
            this.Coord.Name = "Coord";
            this.Coord.Size = new System.Drawing.Size(68, 13);
            this.Coord.TabIndex = 6;
            this.Coord.Text = "X,Y koordin.:";
            // 
            // rgb_val1
            // 
            this.rgb_val1.AutoSize = true;
            this.rgb_val1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.rgb_val1.Location = new System.Drawing.Point(785, 589);
            this.rgb_val1.Name = "rgb_val1";
            this.rgb_val1.Size = new System.Drawing.Size(56, 13);
            this.rgb_val1.TabIndex = 7;
            this.rgb_val1.Text = "RGB PB1:";
            // 
            // rgb_val2
            // 
            this.rgb_val2.AutoSize = true;
            this.rgb_val2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.rgb_val2.Location = new System.Drawing.Point(785, 627);
            this.rgb_val2.Name = "rgb_val2";
            this.rgb_val2.Size = new System.Drawing.Size(56, 13);
            this.rgb_val2.TabIndex = 8;
            this.rgb_val2.Text = "RGB PB2:";
            // 
            // toolTip1
            // 
            this.toolTip1.AutomaticDelay = 200;
            this.toolTip1.BackColor = System.Drawing.SystemColors.Window;
            this.toolTip1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ToolTipTitle = "Pikseļa informācija:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.timeLabel.Location = new System.Drawing.Point(785, 551);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(69, 13);
            this.timeLabel.TabIndex = 9;
            this.timeLabel.Text = "Izpildes laiks:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton5);
            this.groupBox1.Controls.Add(this.radioButton4);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.radioButton3);
            this.groupBox1.Controls.Add(this.U);
            this.groupBox1.Controls.Add(this.YUV);
            this.groupBox1.Controls.Add(this.Y);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.CMYK);
            this.groupBox1.Controls.Add(this.V);
            this.groupBox1.Controls.Add(this.S);
            this.groupBox1.Controls.Add(this.H);
            this.groupBox1.Controls.Add(this.HSV);
            this.groupBox1.Controls.Add(this.B);
            this.groupBox1.Controls.Add(this.G);
            this.groupBox1.Controls.Add(this.R);
            this.groupBox1.Controls.Add(this.RGB);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox1.Location = new System.Drawing.Point(973, 224);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(276, 186);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Krāsu sistēmas";
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(152, 151);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(32, 17);
            this.radioButton5.TabIndex = 16;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "K";
            this.radioButton5.UseVisualStyleBackColor = true;
            this.radioButton5.Click += new System.EventHandler(this.RGB_Click);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(152, 124);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(40, 17);
            this.radioButton4.TabIndex = 15;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Yel";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.Click += new System.EventHandler(this.RGB_Click);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(152, 97);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(34, 17);
            this.radioButton1.TabIndex = 14;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "M";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.Click += new System.EventHandler(this.RGB_Click);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(212, 124);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(32, 17);
            this.radioButton3.TabIndex = 13;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "V";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.Click += new System.EventHandler(this.RGB_Click);
            // 
            // U
            // 
            this.U.AutoSize = true;
            this.U.Location = new System.Drawing.Point(212, 97);
            this.U.Name = "U";
            this.U.Size = new System.Drawing.Size(33, 17);
            this.U.TabIndex = 12;
            this.U.TabStop = true;
            this.U.Text = "U";
            this.U.UseVisualStyleBackColor = true;
            this.U.Click += new System.EventHandler(this.RGB_Click);
            // 
            // YUV
            // 
            this.YUV.AutoSize = true;
            this.YUV.Location = new System.Drawing.Point(212, 33);
            this.YUV.Name = "YUV";
            this.YUV.Size = new System.Drawing.Size(47, 17);
            this.YUV.TabIndex = 11;
            this.YUV.Text = "YUV";
            this.YUV.UseVisualStyleBackColor = true;
            this.YUV.Click += new System.EventHandler(this.RGB_Click);
            // 
            // Y
            // 
            this.Y.AutoSize = true;
            this.Y.Location = new System.Drawing.Point(212, 70);
            this.Y.Name = "Y";
            this.Y.Size = new System.Drawing.Size(32, 17);
            this.Y.TabIndex = 10;
            this.Y.Text = "Y";
            this.Y.UseVisualStyleBackColor = true;
            this.Y.Click += new System.EventHandler(this.RGB_Click);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(152, 70);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(32, 17);
            this.radioButton2.TabIndex = 9;
            this.radioButton2.Text = "C";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.Click += new System.EventHandler(this.RGB_Click);
            // 
            // CMYK
            // 
            this.CMYK.AutoSize = true;
            this.CMYK.Location = new System.Drawing.Point(139, 33);
            this.CMYK.Name = "CMYK";
            this.CMYK.Size = new System.Drawing.Size(55, 17);
            this.CMYK.TabIndex = 8;
            this.CMYK.Text = "CMYK";
            this.CMYK.UseVisualStyleBackColor = true;
            this.CMYK.Click += new System.EventHandler(this.RGB_Click);
            // 
            // V
            // 
            this.V.AutoSize = true;
            this.V.Location = new System.Drawing.Point(85, 124);
            this.V.Name = "V";
            this.V.Size = new System.Drawing.Size(40, 17);
            this.V.TabIndex = 7;
            this.V.Text = "Val";
            this.V.UseVisualStyleBackColor = true;
            this.V.Click += new System.EventHandler(this.RGB_Click);
            // 
            // S
            // 
            this.S.AutoSize = true;
            this.S.Location = new System.Drawing.Point(85, 97);
            this.S.Name = "S";
            this.S.Size = new System.Drawing.Size(41, 17);
            this.S.TabIndex = 6;
            this.S.Text = "Sat";
            this.S.UseVisualStyleBackColor = true;
            this.S.Click += new System.EventHandler(this.RGB_Click);
            // 
            // H
            // 
            this.H.AutoSize = true;
            this.H.Location = new System.Drawing.Point(85, 70);
            this.H.Name = "H";
            this.H.Size = new System.Drawing.Size(45, 17);
            this.H.TabIndex = 5;
            this.H.Text = "Hue";
            this.H.UseVisualStyleBackColor = true;
            this.H.Click += new System.EventHandler(this.RGB_Click);
            // 
            // HSV
            // 
            this.HSV.AutoSize = true;
            this.HSV.Location = new System.Drawing.Point(76, 33);
            this.HSV.Name = "HSV";
            this.HSV.Size = new System.Drawing.Size(47, 17);
            this.HSV.TabIndex = 4;
            this.HSV.Text = "HSV";
            this.HSV.UseVisualStyleBackColor = true;
            this.HSV.Click += new System.EventHandler(this.RGB_Click);
            // 
            // B
            // 
            this.B.AutoSize = true;
            this.B.Location = new System.Drawing.Point(31, 124);
            this.B.Name = "B";
            this.B.Size = new System.Drawing.Size(32, 17);
            this.B.TabIndex = 3;
            this.B.Text = "B";
            this.B.UseVisualStyleBackColor = true;
            this.B.Click += new System.EventHandler(this.RGB_Click);
            // 
            // G
            // 
            this.G.AutoSize = true;
            this.G.Location = new System.Drawing.Point(31, 97);
            this.G.Name = "G";
            this.G.Size = new System.Drawing.Size(33, 17);
            this.G.TabIndex = 2;
            this.G.Text = "G";
            this.G.UseVisualStyleBackColor = true;
            this.G.Click += new System.EventHandler(this.RGB_Click);
            // 
            // R
            // 
            this.R.AutoSize = true;
            this.R.Location = new System.Drawing.Point(31, 70);
            this.R.Name = "R";
            this.R.Size = new System.Drawing.Size(33, 17);
            this.R.TabIndex = 1;
            this.R.Text = "R";
            this.R.UseVisualStyleBackColor = true;
            this.R.Click += new System.EventHandler(this.RGB_Click);
            // 
            // RGB
            // 
            this.RGB.AutoSize = true;
            this.RGB.BackColor = System.Drawing.SystemColors.ControlText;
            this.RGB.Checked = true;
            this.RGB.Location = new System.Drawing.Point(11, 33);
            this.RGB.Name = "RGB";
            this.RGB.Size = new System.Drawing.Size(48, 17);
            this.RGB.TabIndex = 0;
            this.RGB.TabStop = true;
            this.RGB.Text = "RGB";
            this.RGB.UseVisualStyleBackColor = false;
            this.RGB.Click += new System.EventHandler(this.RGB_Click);
            // 
            // C_val1
            // 
            this.C_val1.AutoSize = true;
            this.C_val1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.C_val1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.C_val1.Location = new System.Drawing.Point(26, 18);
            this.C_val1.Name = "C_val1";
            this.C_val1.Size = new System.Drawing.Size(0, 13);
            this.C_val1.TabIndex = 12;
            // 
            // C_val2
            // 
            this.C_val2.AutoSize = true;
            this.C_val2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.C_val2.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.C_val2.Location = new System.Drawing.Point(91, 18);
            this.C_val2.Name = "C_val2";
            this.C_val2.Size = new System.Drawing.Size(0, 13);
            this.C_val2.TabIndex = 13;
            // 
            // C_val3
            // 
            this.C_val3.AutoSize = true;
            this.C_val3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.C_val3.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.C_val3.Location = new System.Drawing.Point(150, 18);
            this.C_val3.Name = "C_val3";
            this.C_val3.Size = new System.Drawing.Size(0, 13);
            this.C_val3.TabIndex = 14;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.C_val1);
            this.groupBox2.Controls.Add(this.C_val3);
            this.groupBox2.Controls.Add(this.C_val2);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.groupBox2.Location = new System.Drawing.Point(1004, 153);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(205, 42);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "RGB vertības";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Ch2);
            this.groupBox3.Controls.Add(this.Ch1);
            this.groupBox3.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.groupBox3.Location = new System.Drawing.Point(973, 416);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(276, 70);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Pikseļa kanālu vērtības";
            // 
            // Ch2
            // 
            this.Ch2.AutoSize = true;
            this.Ch2.Location = new System.Drawing.Point(6, 43);
            this.Ch2.Name = "Ch2";
            this.Ch2.Size = new System.Drawing.Size(37, 13);
            this.Ch2.TabIndex = 1;
            this.Ch2.Text = "CMYK";
            // 
            // Ch1
            // 
            this.Ch1.AutoSize = true;
            this.Ch1.Location = new System.Drawing.Point(6, 21);
            this.Ch1.Name = "Ch1";
            this.Ch1.Size = new System.Drawing.Size(29, 13);
            this.Ch1.TabIndex = 0;
            this.Ch1.Text = "YUV";
            // 
            // Current_label
            // 
            this.Current_label.AutoSize = true;
            this.Current_label.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Current_label.Font = new System.Drawing.Font("Microsoft PhagsPa", 7.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Current_label.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.Current_label.Location = new System.Drawing.Point(1090, 211);
            this.Current_label.Name = "Current_label";
            this.Current_label.Size = new System.Drawing.Size(105, 13);
            this.Current_label.TabIndex = 17;
            this.Current_label.Text = "Aktīvā krāsu sistēma:";
            // 
            // chart1
            // 
            chartArea3.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea3);
            legend3.Enabled = false;
            legend3.Name = "Legend1";
            this.chart1.Legends.Add(legend3);
            this.chart1.Location = new System.Drawing.Point(54, 500);
            this.chart1.Name = "chart1";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.chart1.Series.Add(series3);
            this.chart1.Size = new System.Drawing.Size(279, 160);
            this.chart1.TabIndex = 19;
            this.chart1.Text = "chart1";
            // 
            // chart2
            // 
            chartArea4.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea4);
            legend4.Enabled = false;
            legend4.Name = "Legend1";
            this.chart2.Legends.Add(legend4);
            this.chart2.Location = new System.Drawing.Point(406, 500);
            this.chart2.Name = "chart2";
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.chart2.Series.Add(series4);
            this.chart2.Size = new System.Drawing.Size(307, 160);
            this.chart2.TabIndex = 20;
            this.chart2.Text = "chart2";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1284, 24);
            this.menuStrip1.TabIndex = 21;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filtrs1ToolStripMenuItem,
            this.filtrs2ToolStripMenuItem,
            this.filtrs3ToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(42, 20);
            this.toolStripMenuItem1.Text = "Filtri";
            // 
            // filtrs1ToolStripMenuItem
            // 
            this.filtrs1ToolStripMenuItem.Name = "filtrs1ToolStripMenuItem";
            this.filtrs1ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.filtrs1ToolStripMenuItem.Text = "Filtrs 1";
            this.filtrs1ToolStripMenuItem.Click += new System.EventHandler(this.filtrs1ToolStripMenuItem_Click);
            // 
            // filtrs2ToolStripMenuItem
            // 
            this.filtrs2ToolStripMenuItem.Name = "filtrs2ToolStripMenuItem";
            this.filtrs2ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.filtrs2ToolStripMenuItem.Text = "Filtrs 2";
            // 
            // filtrs3ToolStripMenuItem
            // 
            this.filtrs3ToolStripMenuItem.Name = "filtrs3ToolStripMenuItem";
            this.filtrs3ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.filtrs3ToolStripMenuItem.Text = "Filtrs 3";
            // 
            // IKAA_Lab3_EC154
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1284, 696);
            this.Controls.Add(this.chart2);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.Current_label);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.rgb_val2);
            this.Controls.Add(this.rgb_val1);
            this.Controls.Add(this.Coord);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.open_button);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "IKAA_Lab3_EC154";
            this.Text = "IKAA_Lab3_EC154";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button open_button;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label Coord;
        private System.Windows.Forms.Label rgb_val1;
        private System.Windows.Forms.Label rgb_val2;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton B;
        private System.Windows.Forms.RadioButton G;
        private System.Windows.Forms.RadioButton R;
        private System.Windows.Forms.RadioButton RGB;
        private System.Windows.Forms.RadioButton YUV;
        private System.Windows.Forms.RadioButton Y;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton CMYK;
        private System.Windows.Forms.RadioButton V;
        private System.Windows.Forms.RadioButton S;
        private System.Windows.Forms.RadioButton H;
        private System.Windows.Forms.RadioButton HSV;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton U;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label C_val1;
        private System.Windows.Forms.Label C_val2;
        private System.Windows.Forms.Label C_val3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label Ch1;
        private System.Windows.Forms.Label Current_label;
        private System.Windows.Forms.Label Ch2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem filtrs1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filtrs2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filtrs3ToolStripMenuItem;
    }
}

