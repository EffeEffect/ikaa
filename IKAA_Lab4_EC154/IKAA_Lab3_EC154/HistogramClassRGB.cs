﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;
using System.Data;
using System.Drawing;

namespace IKAA_Lab3_EC154
{
    public class HistogramClassRGB
    {
        //1D masivi
        public int[] R;
        public int[] G;
        public int[] B;
        public int[] I;

        //konstruktors
        public HistogramClassRGB()
        {
            R = new int[257]; //0-255: nolasitas vertibas; 256: max vertiba no iepriekseja un vel nulta vertiba
            G = new int[257];
            B = new int[257];
            I = new int[257];
        }
       
        //attira histogramu
        public void EraseHistogram()
        {
            for(int i = 0; i < 257; i++)//katru histogrammas vertibu uzstada uz nulli
            {
                R[i] = 0;
                G[i] = 0;
                B[i] = 0;
                I[i] = 0;
            }
        }

        //nolasa datus no attela 
        public void ReadHistogram(PixelClassRGB[,] img)
        {
            EraseHistogram();

            //nolasisana no attela 2 ciklos (x un y)
            //skaitam pikselus ar dazadam intensitatem
            for (int x = 0; x < img.GetLength(0); x++)
            {
                for (int y = 0; y < img.GetLength(1); y++)
                {
                    R[img[x, y].R]++;
                    G[img[x, y].G]++;
                    B[img[x, y].B]++;
                    I[img[x, y].I]++;
                }
            }

            //nosaka maksimalo elementu un ieraksta masiva pedeja elementa
            for (int i = 0; i < 256; i++)
            {
                R[256] = Math.Max(R[256], R[i]);
                G[256] = Math.Max(G[256], G[i]);
                B[256] = Math.Max(B[256], B[i]);
                I[256] = Math.Max(I[256], I[i]);
            }
        }
        //zimejam histogramas uz charta
        public void DrawHistogram(Chart chart)
        {
            chart.Series.Clear();
            chart.ChartAreas.Clear();
            //pieliekam klat jaunu chart laukumu un chart serijas(aka grafikus)
            chart.ChartAreas.Add("ChartArea");


            //4grafiki = 4 serijas
            chart.Series.Add("R");
            chart.Series["R"].Color = Color.Red;

            chart.Series.Add("G");
            chart.Series["G"].Color = Color.Green;

            chart.Series.Add("B");
            chart.Series["B"].Color = Color.Blue;

            chart.Series.Add("I");
            chart.Series["I"].Color = Color.Black;

            //datu ievietosana grafikos(serijas)
            //256 punkti ko ievieto no histogramas

            for (int i = 0; i < 256; i++)
            {
                chart.Series["R"].Points.AddXY(i, R[i]);
                chart.Series["G"].Points.AddXY(i, G[i]);
                chart.Series["B"].Points.AddXY(i, B[i]);
                chart.Series["I"].Points.AddXY(i, I[i]);
            }

        }
    }
}
