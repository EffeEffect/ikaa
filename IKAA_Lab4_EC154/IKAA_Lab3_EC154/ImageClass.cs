﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IKAA_Lab3_EC154
{
    public class ImageClass
    {
        public PixelClassRGB[,] img1; //originalas vertibas
        public PixelClassRGB[,] img2; //parveidotas vertibas
        public PixelClassHSV[,] imgHSV;
        public PixelClassCMYK[,] imgCMYK;
        public PixelClassYUV[,] imgYUV;

        public HistogramClassRGB hst1;
        public HistogramClassRGB hst2;

        public void ReadImage(Bitmap bmp)
        {
            //veidojam masivus
            img1 = new PixelClassRGB[bmp.Width, bmp.Height];
            img2 = new PixelClassRGB[bmp.Width, bmp.Height];
            imgHSV = new PixelClassHSV[bmp.Width, bmp.Height];
            imgCMYK = new PixelClassCMYK[bmp.Width, bmp.Height];
            imgYUV = new PixelClassYUV[bmp.Width, bmp.Height];

            hst1 = new HistogramClassRGB();
            hst2 = new HistogramClassRGB();

            //ieslegt atmina bitmapu un to atvert ar skenesanu
            var bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, bmp.PixelFormat);
            IntPtr ptr = IntPtr.Zero; //pointer lai varetu lasit rindas

            //cik daudz bitu ir pikseli jeb cik kanalu
            int pixelComponents; //RGB, RGBA
            if (bmpData.PixelFormat == PixelFormat.Format24bppRgb)
            {
                pixelComponents = 3;
            }
            else if (bmpData.PixelFormat == PixelFormat.Format32bppArgb)
            {
                pixelComponents = 4;
            }
            else //ja ir cits formats
            {
                pixelComponents = 0;
            }

            var row = new byte[bmp.Width * pixelComponents]; //viena attela rinda
            for (int y = 0; y < bmp.Height; y++) //skenesana
            {
                ptr = bmpData.Scan0 + y * bmpData.Stride; //skenejam rindas
                Marshal.Copy(ptr, row, 0, row.Length); //kopejam info no rindas
                for (int x = 0; x < bmp.Width; x++)
                {
                    img1[x, y] = new PixelClassRGB(
                        row[pixelComponents * x + 2],
                        row[pixelComponents * x + 1],
                        row[pixelComponents * x]);         //izveid.jauno pixeli un ierakstam tur vertibas no rindas

                    img2[x, y] = new PixelClassRGB(
                        row[pixelComponents * x + 2],
                        row[pixelComponents * x + 1],
                        row[pixelComponents * x]);         //izveid.jauno pixeli un ierakstam tur vertibas no rindas

                    imgHSV[x, y] = new PixelClassHSV(
                        img1[x, y].R,
                        img1[x, y].G,
                        img1[x, y].B);

                    imgCMYK[x, y] = new PixelClassCMYK(
                        img1[x, y].R,
                        img1[x, y].G,
                        img1[x, y].B);

                     imgYUV[x, y] = new PixelClassYUV(
                        img1[x, y].R,
                        img1[x, y].G,
                        img1[x, y].B);
                }
            }
            bmp.UnlockBits(bmpData);
            hst1.ReadHistogram(img1);
        }

        int[,] FiltrArray1 = new int[,] { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } }; //define filtra1 masivu 
        int[,] FiltrArray2 = new int[,] { { 1, 1, 1 }, { 1, 2, 1 }, { 1, 1, 1 } }; //define filtra2 masivu 
        int[,] FiltrArray3 = new int[,] { { 1, 2, 1 }, { 2, 4, 2 }, { 1, 2, 1 } }; //define filtra3 masivu
        public void Filter() 
        {
            int sum = 0;
            for ( int i=1; i < 3; i++ ) {
                for ( int j=1; j < 3; j++ ) {
                    sum += FiltrArray1[i,j];
                }
            }   
            
            for (int x = 1; x < (img1.GetLength(x) + 1); x--)
            {
                for (int y = 1; y < (img1.GetLength(y) + 1); y--)
                {
                    int r = img1[x-1,y-1].R * FiltrArray1[0,0] * FiltrArray1[0, 1] * FiltrArray1[0, 2] 
                                          * FiltrArray1[1, 0] * FiltrArray1[1, 1] * FiltrArray1[1, 2] 
                                          * FiltrArray1[2, 0] * FiltrArray1[2, 1] * FiltrArray1[2, 2];
                    int g = img1[x-1,y-1].G * FiltrArray1[0, 0] * FiltrArray1[0, 1] * FiltrArray1[0, 2]
                                          * FiltrArray1[1, 0] * FiltrArray1[1, 1] * FiltrArray1[1, 2]
                                          * FiltrArray1[2, 0] * FiltrArray1[2, 1] * FiltrArray1[2, 2];
                    int b = img1[x-1,y-1].B * FiltrArray1[0, 0] * FiltrArray1[0, 1] * FiltrArray1[0, 2]
                                          * FiltrArray1[1, 0] * FiltrArray1[1, 1] * FiltrArray1[1, 2]
                                          * FiltrArray1[2, 0] * FiltrArray1[2, 1] * FiltrArray1[2, 2];

                    img2[x, y].R = (byte)(r/sum);
                    img2[x, y].G = (byte)(g/sum);
                    img2[x, y].B = (byte)(b/sum);
                }
            }
            
        }

      

    public Bitmap DrawImage(PixelClassRGB[,] img, string mode) //zimejam masivu attelam 
        {
            if (img != null)
            {
                IntPtr ptr = IntPtr.Zero; //mainigie
                var bmp = new Bitmap(img.GetLength(0), img.GetLength(1), PixelFormat.Format24bppRgb);
                var bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);
                var row = new byte[bmp.Width * 3]; //jo ir 24 biti

                for (int y = 0; y < bmp.Height; y++)
                {
                    for (int x = 0; x < bmp.Width; x++)
                    {
                        switch (mode)
                        {
                            case "RGB":
                                {
                                    row[3 * x + 2] = img[x, y].R;
                                    row[3 * x + 1] = img[x, y].G;
                                    row[3 * x] = img[x, y].B;
                                    break;
                                }
                            case "R":
                                {
                                    row[3 * x + 2] = img[x, y].R;
                                    row[3 * x + 1] = 0;
                                    row[3 * x] = 0;
                                    break;
                                }
                            case "G":
                                {
                                    row[3 * x + 2] = 0;
                                    row[3 * x + 1] = img[x, y].G;
                                    row[3 * x] = 0;
                                    break;
                                }
                            case "B":
                                {
                                    row[3 * x + 2] = 0;
                                    row[3 * x + 1] = 0;
                                    row[3 * x] = img[x, y].B;
                                    break;
                                }
                            case "HSV":
                                {
                                    row[3 * x + 2] = img[x, y].HSVtoRGB(imgHSV[x, y].H,
                                                                       imgHSV[x, y].S,
                                                                       imgHSV[x, y].V).R;
                                    row[3 * x + 1] = img[x, y].HSVtoRGB(imgHSV[x, y].H,
                                                                        imgHSV[x, y].S,
                                                                        imgHSV[x, y].V).G;
                                    row[3 * x] = img[x, y].HSVtoRGB(imgHSV[x, y].H,
                                                                    imgHSV[x, y].S,
                                                                    imgHSV[x, y].V).B;
                                    break;
                                }
                            case "Hue":
                                {
                                    row[3 * x + 2] = img[x, y].HSVtoRGB(imgHSV[x, y].H, 255, 255).R;
                                    row[3 * x + 1] = img[x, y].HSVtoRGB(imgHSV[x, y].H, 255, 255).G;
                                    row[3 * x] = img[x, y].HSVtoRGB(imgHSV[x, y].H, 255, 255).B;
                                    break;
                                }
                            case "Sat":
                                {
                                    row[3 * x + 2] = imgHSV[x, y].S;
                                    row[3 * x + 1] = imgHSV[x, y].S;
                                    row[3 * x] = imgHSV[x, y].S;
                                    break;
                                }
                            case "Val":
                                {
                                    row[3 * x + 2] = imgHSV[x, y].V;
                                    row[3 * x + 1] = imgHSV[x, y].V;
                                    row[3 * x] = imgHSV[x, y].V;
                                    break;
                                }
                            case "CMYK":
                                {
                                    row[3 * x + 2] = img1[x, y].CMYKtoRGB(imgCMYK[x, y].C,
                                                                    imgCMYK[x, y].M,
                                                                    imgCMYK[x, y].Y,
                                                                    imgCMYK[x, y].K).R;

                                    row[3 * x + 1] = img1[x, y].CMYKtoRGB(imgCMYK[x, y].C,
                                                                    imgCMYK[x, y].M,
                                                                    imgCMYK[x, y].Y,
                                                                    imgCMYK[x, y].K).G;

                                    row[3 * x] = img1[x, y].CMYKtoRGB(imgCMYK[x, y].C,
                                                                    imgCMYK[x, y].M,
                                                                    imgCMYK[x, y].Y,
                                                                    imgCMYK[x,y].K).B;
                                    break;
                                }
                            case "C":
                                {
                                    row[3 * x + 2] = img[x, y].CMYKtoRGB(imgCMYK[x, y].C, 0, 0, 0).R;
                                    row[3 * x + 1] = img[x, y].CMYKtoRGB(imgCMYK[x, y].C, 0, 0, 0).G;
                                    row[3 * x] = img[x, y].CMYKtoRGB(imgCMYK[x, y].C, 0, 0, 0).B;
                                    break;
                                }
                            case "M":
                                {
                                    row[3 * x + 2] = img[x, y].CMYKtoRGB(0, imgCMYK[x, y].M, 0, 0).R;
                                    row[3 * x + 1] = img[x, y].CMYKtoRGB(0, imgCMYK[x, y].M, 0, 0).G;
                                    row[3 * x] = img[x, y].CMYKtoRGB(0, imgCMYK[x, y].M, 0, 0).B;
                                    break;
                                }
                            case "Yel":
                                {
                                    row[3 * x + 2] = img[x, y].CMYKtoRGB(0, 0, imgCMYK[x, y].Y, 0).R;
                                    row[3 * x + 1] = img[x, y].CMYKtoRGB(0, 0, imgCMYK[x, y].Y, 0).G;
                                    row[3 * x] = img[x, y].CMYKtoRGB(20, 0, imgCMYK[x, y].Y, 0).B;
                                    break;
                                }
                            case "K":
                                {
                                    row[3 * x + 2] = img[x, y].CMYKtoRGB(0, 0, 0, imgCMYK[x, y].K).R;
                                    row[3 * x + 1] = img[x, y].CMYKtoRGB(0, 0, 0, imgCMYK[x, y].K).G;
                                    row[3 * x] = img[x, y].CMYKtoRGB(0, 0, 0, imgCMYK[x, y].K).B;
                                    break;
                                }

                            case "YUV":
                                {
                                    row[3 * x + 2] = img1[x, y].YUVtoRGB(imgYUV[x, y].Y,
                                                                        imgYUV[x, y].U,
                                                                        imgYUV[x, y].V).R;

                                    row[3 * x + 1] = img1[x, y].YUVtoRGB(imgYUV[x, y].Y,
                                                                        imgYUV[x, y].U,
                                                                        imgYUV[x, y].V).G;

                                    row[3 * x] = img1[x, y].YUVtoRGB(imgYUV[x, y].Y,
                                                                        imgYUV[x, y].U,
                                                                        imgYUV[x, y].V).B;
                                    break;
                                }
                            case "Y":
                                {
                                    row[3 * x + 2] = img[x, y].YUVtoRGB(imgYUV[x, y].Y, 0, 0).R;
                                    row[3 * x + 1] = img[x, y].YUVtoRGB(imgYUV[x, y].Y, 0, 0).G;
                                    row[3 * x] = img[x, y].YUVtoRGB(imgYUV[x, y].Y, 0, 0).B;
                                    break;
                                }
                            case "U":
                                {
                                    row[3 * x + 2] = img[x, y].YUVtoRGB(0, imgYUV[x, y].U, 0).R;
                                    row[3 * x + 1] = img[x, y].YUVtoRGB(0, imgYUV[x, y].U, 0).G;
                                    row[3 * x] = img[x, y].YUVtoRGB(0, imgYUV[x, y].U, 0).B;
                                    break;
                                }
                            case "V":
                                {
                                    row[3 * x + 2] = img[x, y].YUVtoRGB(0, 0, imgYUV[x, y].V).R;
                                    row[3 * x + 1] = img[x, y].YUVtoRGB(0, 0, imgYUV[x, y].V).G;
                                    row[3 * x] = img[x, y].YUVtoRGB(0, 0, imgYUV[x, y].V).B;
                                    break;
                                }
                        }
                    }
                    ptr = bmpData.Scan0 + y * bmpData.Stride;
                    Marshal.Copy(row, 0, ptr, row.Length);
                }
                bmp.UnlockBits(bmpData);
                return bmp;
            }
            else { return null; }
        }
    }
}

