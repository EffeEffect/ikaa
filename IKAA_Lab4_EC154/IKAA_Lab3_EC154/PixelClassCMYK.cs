﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IKAA_Lab3_EC154
{
    public class PixelClassCMYK
    {
        public float C; //0-359-0
        public float M; //0....255
        public float Y; //0....255
        public float K; //0....255

        public PixelClassCMYK()
        {
            C = 0;
            M = 0;
            Y = 0;
            K = 0;
        }

        //konstruktors + pareja no RGB uz CMYK
        public PixelClassCMYK(byte r, byte g, byte b)
        { 
            float rf;
            float gf;
            float bf;

            rf = r / 255F;
            gf = g / 255F;
            bf = b / 255F;

            K = CMYKcheck(1 - Math.Max(rf, Math.Max(gf,bf)));
            C = CMYKcheck((1 - rf - K) / (1 - K));
            M = CMYKcheck((1 - gf - K) / (1 - K));
            Y = CMYKcheck((1 - bf - K) / (1 - K));

        }
            private static float CMYKcheck(float value)
            {
                if (value < 0 || float.IsNaN(value))
                {
                    value = 0;
                }
                return value;
        }
    }
}
