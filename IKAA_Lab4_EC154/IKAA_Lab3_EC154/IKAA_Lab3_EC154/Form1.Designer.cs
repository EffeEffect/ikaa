﻿namespace IKAA_Lab3_EC154
{
    partial class IKAA_Lab3_EC154
    {
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IKAA_Lab3_EC154));
            this.open_button = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button4 = new System.Windows.Forms.Button();
            this.Coord = new System.Windows.Forms.Label();
            this.rgb_val1 = new System.Windows.Forms.Label();
            this.rgb_val2 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.timeLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.U = new System.Windows.Forms.RadioButton();
            this.YUV = new System.Windows.Forms.RadioButton();
            this.Y = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.CMYK = new System.Windows.Forms.RadioButton();
            this.V = new System.Windows.Forms.RadioButton();
            this.S = new System.Windows.Forms.RadioButton();
            this.H = new System.Windows.Forms.RadioButton();
            this.HSV = new System.Windows.Forms.RadioButton();
            this.B = new System.Windows.Forms.RadioButton();
            this.G = new System.Windows.Forms.RadioButton();
            this.R = new System.Windows.Forms.RadioButton();
            this.RGB = new System.Windows.Forms.RadioButton();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.C_val1 = new System.Windows.Forms.Label();
            this.C_val2 = new System.Windows.Forms.Label();
            this.C_val3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // open_button
            // 
            this.open_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.open_button.Location = new System.Drawing.Point(1004, 28);
            this.open_button.Name = "open_button";
            this.open_button.Size = new System.Drawing.Size(205, 29);
            this.open_button.TabIndex = 0;
            this.open_button.Text = "OPEN";
            this.open_button.UseVisualStyleBackColor = true;
            this.open_button.Click += new System.EventHandler(this.open_button_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(1004, 72);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(205, 29);
            this.button2.TabIndex = 1;
            this.button2.Text = "SAVE";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(1004, 118);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(205, 29);
            this.button3.TabIndex = 2;
            this.button3.Text = "INVERT";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox1.Location = new System.Drawing.Point(35, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(450, 450);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox2.Location = new System.Drawing.Point(506, 30);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(450, 450);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseDown);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(1093, 491);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(43, 38);
            this.button4.TabIndex = 5;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Coord
            // 
            this.Coord.AutoSize = true;
            this.Coord.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Coord.Location = new System.Drawing.Point(503, 491);
            this.Coord.Name = "Coord";
            this.Coord.Size = new System.Drawing.Size(68, 13);
            this.Coord.TabIndex = 6;
            this.Coord.Text = "X,Y koordin.:";
            // 
            // rgb_val1
            // 
            this.rgb_val1.AutoSize = true;
            this.rgb_val1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.rgb_val1.Location = new System.Drawing.Point(32, 491);
            this.rgb_val1.Name = "rgb_val1";
            this.rgb_val1.Size = new System.Drawing.Size(56, 13);
            this.rgb_val1.TabIndex = 7;
            this.rgb_val1.Text = "RGB PB1:";
            // 
            // rgb_val2
            // 
            this.rgb_val2.AutoSize = true;
            this.rgb_val2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.rgb_val2.Location = new System.Drawing.Point(32, 529);
            this.rgb_val2.Name = "rgb_val2";
            this.rgb_val2.Size = new System.Drawing.Size(56, 13);
            this.rgb_val2.TabIndex = 8;
            this.rgb_val2.Text = "RGB PB2:";
            // 
            // toolTip1
            // 
            this.toolTip1.AutomaticDelay = 200;
            this.toolTip1.BackColor = System.Drawing.SystemColors.Window;
            this.toolTip1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ToolTipTitle = "Pikseļa informācija:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.timeLabel.Location = new System.Drawing.Point(503, 529);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(69, 13);
            this.timeLabel.TabIndex = 9;
            this.timeLabel.Text = "Izpildes laiks:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton5);
            this.groupBox1.Controls.Add(this.radioButton4);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.radioButton3);
            this.groupBox1.Controls.Add(this.U);
            this.groupBox1.Controls.Add(this.YUV);
            this.groupBox1.Controls.Add(this.Y);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.CMYK);
            this.groupBox1.Controls.Add(this.V);
            this.groupBox1.Controls.Add(this.S);
            this.groupBox1.Controls.Add(this.H);
            this.groupBox1.Controls.Add(this.HSV);
            this.groupBox1.Controls.Add(this.B);
            this.groupBox1.Controls.Add(this.G);
            this.groupBox1.Controls.Add(this.R);
            this.groupBox1.Controls.Add(this.RGB);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox1.Location = new System.Drawing.Point(973, 224);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(276, 186);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Krāsu sistēmas";
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(152, 151);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(32, 17);
            this.radioButton5.TabIndex = 16;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "K";
            this.radioButton5.UseVisualStyleBackColor = true;
            this.radioButton5.Click += new System.EventHandler(this.RGB_Click);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(152, 124);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(40, 17);
            this.radioButton4.TabIndex = 15;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Yel";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.Click += new System.EventHandler(this.RGB_Click);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(152, 97);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(34, 17);
            this.radioButton1.TabIndex = 14;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "M";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.Click += new System.EventHandler(this.RGB_Click);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(212, 124);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(32, 17);
            this.radioButton3.TabIndex = 13;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "V";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.Click += new System.EventHandler(this.RGB_Click);
            // 
            // U
            // 
            this.U.AutoSize = true;
            this.U.Location = new System.Drawing.Point(212, 97);
            this.U.Name = "U";
            this.U.Size = new System.Drawing.Size(33, 17);
            this.U.TabIndex = 12;
            this.U.TabStop = true;
            this.U.Text = "U";
            this.U.UseVisualStyleBackColor = true;
            this.U.Click += new System.EventHandler(this.RGB_Click);
            // 
            // YUV
            // 
            this.YUV.AutoSize = true;
            this.YUV.Location = new System.Drawing.Point(212, 33);
            this.YUV.Name = "YUV";
            this.YUV.Size = new System.Drawing.Size(47, 17);
            this.YUV.TabIndex = 11;
            this.YUV.Text = "YUV";
            this.YUV.UseVisualStyleBackColor = true;
            this.YUV.Click += new System.EventHandler(this.RGB_Click);
            // 
            // Y
            // 
            this.Y.AutoSize = true;
            this.Y.Location = new System.Drawing.Point(212, 70);
            this.Y.Name = "Y";
            this.Y.Size = new System.Drawing.Size(32, 17);
            this.Y.TabIndex = 10;
            this.Y.Text = "Y";
            this.Y.UseVisualStyleBackColor = true;
            this.Y.Click += new System.EventHandler(this.RGB_Click);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(152, 70);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(32, 17);
            this.radioButton2.TabIndex = 9;
            this.radioButton2.Text = "C";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.Click += new System.EventHandler(this.RGB_Click);
            // 
            // CMYK
            // 
            this.CMYK.AutoSize = true;
            this.CMYK.Location = new System.Drawing.Point(139, 33);
            this.CMYK.Name = "CMYK";
            this.CMYK.Size = new System.Drawing.Size(55, 17);
            this.CMYK.TabIndex = 8;
            this.CMYK.Text = "CMYK";
            this.CMYK.UseVisualStyleBackColor = true;
            this.CMYK.Click += new System.EventHandler(this.RGB_Click);
            // 
            // V
            // 
            this.V.AutoSize = true;
            this.V.Location = new System.Drawing.Point(85, 124);
            this.V.Name = "V";
            this.V.Size = new System.Drawing.Size(40, 17);
            this.V.TabIndex = 7;
            this.V.Text = "Val";
            this.V.UseVisualStyleBackColor = true;
            this.V.Click += new System.EventHandler(this.RGB_Click);
            // 
            // S
            // 
            this.S.AutoSize = true;
            this.S.Location = new System.Drawing.Point(85, 97);
            this.S.Name = "S";
            this.S.Size = new System.Drawing.Size(41, 17);
            this.S.TabIndex = 6;
            this.S.Text = "Sat";
            this.S.UseVisualStyleBackColor = true;
            this.S.Click += new System.EventHandler(this.RGB_Click);
            // 
            // H
            // 
            this.H.AutoSize = true;
            this.H.Location = new System.Drawing.Point(85, 70);
            this.H.Name = "H";
            this.H.Size = new System.Drawing.Size(45, 17);
            this.H.TabIndex = 5;
            this.H.Text = "Hue";
            this.H.UseVisualStyleBackColor = true;
            this.H.Click += new System.EventHandler(this.RGB_Click);
            // 
            // HSV
            // 
            this.HSV.AutoSize = true;
            this.HSV.Location = new System.Drawing.Point(76, 33);
            this.HSV.Name = "HSV";
            this.HSV.Size = new System.Drawing.Size(47, 17);
            this.HSV.TabIndex = 4;
            this.HSV.Text = "HSV";
            this.HSV.UseVisualStyleBackColor = true;
            this.HSV.Click += new System.EventHandler(this.RGB_Click);
            // 
            // B
            // 
            this.B.AutoSize = true;
            this.B.Location = new System.Drawing.Point(31, 124);
            this.B.Name = "B";
            this.B.Size = new System.Drawing.Size(32, 17);
            this.B.TabIndex = 3;
            this.B.Text = "B";
            this.B.UseVisualStyleBackColor = true;
            this.B.Click += new System.EventHandler(this.RGB_Click);
            // 
            // G
            // 
            this.G.AutoSize = true;
            this.G.Location = new System.Drawing.Point(31, 97);
            this.G.Name = "G";
            this.G.Size = new System.Drawing.Size(33, 17);
            this.G.TabIndex = 2;
            this.G.Text = "G";
            this.G.UseVisualStyleBackColor = true;
            this.G.Click += new System.EventHandler(this.RGB_Click);
            // 
            // R
            // 
            this.R.AutoSize = true;
            this.R.Location = new System.Drawing.Point(31, 70);
            this.R.Name = "R";
            this.R.Size = new System.Drawing.Size(33, 17);
            this.R.TabIndex = 1;
            this.R.Text = "R";
            this.R.UseVisualStyleBackColor = true;
            this.R.Click += new System.EventHandler(this.RGB_Click);
            // 
            // RGB
            // 
            this.RGB.AutoSize = true;
            this.RGB.BackColor = System.Drawing.SystemColors.ControlText;
            this.RGB.Checked = true;
            this.RGB.Location = new System.Drawing.Point(11, 33);
            this.RGB.Name = "RGB";
            this.RGB.Size = new System.Drawing.Size(48, 17);
            this.RGB.TabIndex = 0;
            this.RGB.TabStop = true;
            this.RGB.Text = "RGB";
            this.RGB.UseVisualStyleBackColor = false;
            this.RGB.Click += new System.EventHandler(this.RGB_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(1022, 435);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(186, 149);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 11;
            this.pictureBox3.TabStop = false;
            // 
            // C_val1
            // 
            this.C_val1.AutoSize = true;
            this.C_val1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.C_val1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.C_val1.Location = new System.Drawing.Point(26, 18);
            this.C_val1.Name = "C_val1";
            this.C_val1.Size = new System.Drawing.Size(0, 13);
            this.C_val1.TabIndex = 12;
            // 
            // C_val2
            // 
            this.C_val2.AutoSize = true;
            this.C_val2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.C_val2.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.C_val2.Location = new System.Drawing.Point(91, 18);
            this.C_val2.Name = "C_val2";
            this.C_val2.Size = new System.Drawing.Size(0, 13);
            this.C_val2.TabIndex = 13;
            // 
            // C_val3
            // 
            this.C_val3.AutoSize = true;
            this.C_val3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.C_val3.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.C_val3.Location = new System.Drawing.Point(150, 18);
            this.C_val3.Name = "C_val3";
            this.C_val3.Size = new System.Drawing.Size(0, 13);
            this.C_val3.TabIndex = 14;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.C_val1);
            this.groupBox2.Controls.Add(this.C_val3);
            this.groupBox2.Controls.Add(this.C_val2);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.groupBox2.Location = new System.Drawing.Point(1004, 166);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(205, 42);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "RGB vertības";
            // 
            // IKAA_Lab3_EC154
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1261, 608);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.rgb_val2);
            this.Controls.Add(this.rgb_val1);
            this.Controls.Add(this.Coord);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.open_button);
            this.Controls.Add(this.pictureBox3);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "IKAA_Lab3_EC154";
            this.Text = "IKAA_Lab3_EC154";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button open_button;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label Coord;
        private System.Windows.Forms.Label rgb_val1;
        private System.Windows.Forms.Label rgb_val2;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton B;
        private System.Windows.Forms.RadioButton G;
        private System.Windows.Forms.RadioButton R;
        private System.Windows.Forms.RadioButton RGB;
        private System.Windows.Forms.RadioButton YUV;
        private System.Windows.Forms.RadioButton Y;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton CMYK;
        private System.Windows.Forms.RadioButton V;
        private System.Windows.Forms.RadioButton S;
        private System.Windows.Forms.RadioButton H;
        private System.Windows.Forms.RadioButton HSV;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton U;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label C_val1;
        private System.Windows.Forms.Label C_val2;
        private System.Windows.Forms.Label C_val3;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}

