﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IKAA_Lab3_EC154
{
    public class PixelClassHSV
    {
        public int H; //0-359-0
        public byte S; //0....255
        public byte V; //0....255

        public PixelClassHSV()
        {
            H = 0;
            S = 0;
            V = 0;
        }

        //konstruktors + pareja no RGB uz HSV

        public PixelClassHSV(byte r, byte g, byte b)
        {
            int max = Math.Max(r, Math.Max(g, b));
            int min = Math.Min(r, Math.Min(g, b));

            if (max == min) { H = 0; }
            else if ((max == r) && (g >= b))
            { H = 60 * (g - b) / (max - min); }
            else if ((max == r) && (g < b))
            { H = 60 * (g - b) / (max - min) + 360; }
            else if ((max == g))
            { H = 60 * (b - r) / (max - min) + 120; }
            else
            { H = 60 * (r - g) / (max - min) + 240; }
            if (H >= 360)
            { H = 0; }

            if (max == 0) { S = 0; }
            else { S = Convert.ToByte(255 * (1 - ((float)min / max))); }
            V = (byte)max;
        }
    }
}
